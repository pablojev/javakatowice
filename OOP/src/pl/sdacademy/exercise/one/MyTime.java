package pl.sdacademy.exercise.one;

public class MyTime {

	private int hour;
	private int minute;
	private int second;

	public MyTime() { }

	public MyTime(int hour, int minute, int second) {
		this.setTime(hour, minute, second);
	}
	
	public void setTime(int hour, int minute, int second) {
		this.setHour(hour);
		this.setMinute(minute);
		this.setSecond(second);
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if(hour < 24 && hour >= 0) {
			this.hour = hour;
		}
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		if(minute < 60 && minute >= 0) {
			this.minute = minute;
		}
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if(second < 60 && second >= 0) {
			this.second = second;
		}
	}
	
	public String toString() {
		return this.leadZero(this.hour) + ":" 
				+ this.leadZero(this.minute) + ":"
				+ this.leadZero(this.second);
	}
	
	private String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		}
		return "" + num; // przerzutowanie int'a na String
	}
	
	private MyTime next(int hour, int minute, int second) {
		
		if(second == 60) {
			second = 0;
			minute++;
		}
		
		if(minute == 60) {
			minute = 0;
			hour++;
		}
		
		if(hour == 24) {
			hour = 0;
		}
		
		return new MyTime(hour, minute, second);
	}
	
	public MyTime nextSecond() {
		return this.next(this.hour, this.minute, this.second + 1); 
	}
	
	public MyTime nextMinute() {
		return this.next(this.hour, this.minute + 1, this.second);
	}
	
	public MyTime nextHour() {
		return this.next(this.hour + 1, this.minute, this.second);
	}
	
	private MyTime prev(int hour, int minute, int second) {
		
		if(second < 0) {
			second = 59;
			minute--;
		}
		
		if(minute < 0) {
			minute = 59;
			hour--;
		}
		
		if(hour < 0) {
			hour = 23;
		}
		
		return new MyTime(hour, minute, second);
	}

	public MyTime prevSecond() {
		return this.prev(this.hour, this.minute, this.second - 1);
	}
	
	public MyTime prevMinute() {
		return this.prev(this.hour, this.minute - 1, this.second);
	}
	
	public MyTime prevHour() {
		return this.prev(this.hour - 1, this.minute, this.second);
	}
	
}
