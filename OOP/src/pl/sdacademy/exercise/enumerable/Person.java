package pl.sdacademy.exercise.enumerable;

public class Person {
	private String name;
	private PersonSex sex;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PersonSex getSex() {
		return sex;
	}
	public void setSex(PersonSex sex) {
		this.sex = sex;
	}
	@Override
	public String toString() {
		return "Person [name=" + this.name + ", sex=" + this.sex.getSexName() + "]";
	}
	
	
	
	
}
