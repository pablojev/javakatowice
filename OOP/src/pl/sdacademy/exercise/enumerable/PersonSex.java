package pl.sdacademy.exercise.enumerable;

public enum PersonSex {
	MALE("mężczyzna"),
	FEMALE("kobieta"),
	UNKNOWN("nieznany");
	
	private String sexName;
	
	private PersonSex(String sexName) {
		this.sexName = sexName;
	}
	
	public String getSexName() {
		return this.sexName;
	}
}
