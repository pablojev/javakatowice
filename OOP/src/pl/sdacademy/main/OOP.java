package pl.sdacademy.main;

import pl.sdacademy.enumerable.ColorType;
import pl.sdacademy.enumerable.Colors;
import pl.sdacademy.enums.game.Game;
import pl.sdacademy.enums.one.EyeColor;
import pl.sdacademy.enums.one.Human;
import pl.sdacademy.enums.two.Car;
import pl.sdacademy.abstractive.EnglishPerson;
import pl.sdacademy.abstractive.Person;
import pl.sdacademy.abstractive.PolskiPerson;
import pl.sdacademy.abstractive.RussianPerson;
import pl.sdacademy.exercise.enumerable.PersonSex;
import pl.sdacademy.exercise.one.MyTime;
import pl.sdacademy.oop.Leaf;
import pl.sdacademy.oop.MapleLeaf;
import pl.sdacademy.oop.YellowMapleLeaf;

public class OOP {

	public static void main(String[] args) {
		
		new Game().start();
		
		System.exit(0);
		
		// enum.exercise.one
		Human h = new Human("Paweł", EyeColor.BROWN);
		
		System.out.println("name=" + h.getName() + ",eye=" + h.getEye());
		
		// enum.exercise.two
		
		for(Car car : Car.values()) {
			System.out.println("Samochód: " + car + ", rok produkcji: " + car.getYear());
		}
		
		int xx = 17;
		
		if(xx == 17) {
			xx = 20;
		} else {
			xx = 25;
		}
		
		//xx = (warunek) ? wartość_jesli_prawda : wartość_jesli_falsz;
		xx = (xx == 17) ? 20 : 25;
		
		
		
		
		EnglishPerson en = new EnglishPerson("John");
		PolskiPerson pl = new PolskiPerson("Janusz");
		
		Person[] all = { en, pl, new RussianPerson("Sasza") };
		
		for(Person p : all) {
			p.greet();
		}
		
		System.exit(0);
		
//		Person pers = new Person();
//		pers.setName("Paweł");
//		pers.setSex(PersonSex.MALE);
//		
//		System.out.println(pers);
		
		
		Colors cc = new Colors();

		cc.setColor(ColorType.RED);
		
		System.out.println(ColorType.BLUE.getId() + " "
				+ ColorType.RED.getColorName());
		
		
		System.out.println("Twoj kolor to: " + cc.getColor());
		
		
		
		MapleLeaf mpl = new MapleLeaf();
		
		mpl.color = "zolty";
		
		mpl.showMeFlying();
		
		if(mpl instanceof Object) {
			System.out.println("Pochodze od objectu!");
		}
		
		if(mpl instanceof Leaf) {
			System.out.println("Pochodze od liscia!");
		}
		
		if(mpl instanceof YellowMapleLeaf) {
			System.out.println("Pochodze od YellowMapleLeaf!");
		}
		
		
		
		MyTime czas = new MyTime(23,59,59);
		System.out.println(czas);
		
		MyTime kolejnaSekunda = czas.nextMinute();
		System.out.println(kolejnaSekunda);
		
		
		System.out.println("Witam");
		
		// deklarujemy liczbę typu int - jest to typ prosty, 
		// na którym nie mogę użyć metod z klasy Object
		// np. toString();
		int a = 5;
		
		// Poniżej zadeklarowaliśmy dwie zmienne typu Integer
		// Integer jest tzw. klasą osłonową - oznacza to, 
		// że reprezentuje typ prosty za pomocą typu obiketowego
		// który już pozwoli nam na użycie metod pochodzących z klasy Object
		Integer b = 7;
		Integer c = 8;
		
		// Tworzymy nowy obiekt 'lisc'
		// używamy do tego konstruktora bezparametrowego
		Leaf lisc = new Leaf();
		
		// Drukujemy wynik konkatenacji na dwóch ciągach znaków
		// gdyż metoda toString() będzie reprezentowała naszą liczbę
		// jako łancuch znaków (string)
		System.out.println(b.toString() + c.toString());
		
		// Tworzymy obiekt nowego liścia
		Leaf liscPierwszy = new Leaf();
		// i uzupełniamy jego pola wartościami
		// możemy powiedzieć, że są to 'własności obiektu'
		liscPierwszy.name = "Kinga";
		liscPierwszy.color = "zielony";
		liscPierwszy.size = "mały";
		// wywołujemy metodę obiektu
		liscPierwszy.fly();
		
		Leaf liscDrugi = new Leaf();
		liscDrugi.name = "Ania";
		liscDrugi.color = "brązowy";
		liscDrugi.size = "duży";
		liscDrugi.rot();
		
		// tym razem tworząc obiekt nowego liścia wykorzystujemy 
		// konstruktor z parametrem, który pozwoli na 
		// ustawienie wartości pola przekazanym argumentem
		Leaf liscTrzeci = new Leaf("Anonim");
		
		// Drukujemy kolory naszych 3 uprzednio utworzonych liści
		// zwróćmy uwagę, że liść trzeci nie posiada ustawionych wartości w polach
		// dlatego jako jego kolor zosatnie wydrukowana wartość null
		System.out.println(liscPierwszy.whatIsMyColour() 
				+ "\n" + liscDrugi.whatIsMyColour() 
				+ "\n" + liscTrzeci.whatIsMyColour() );
		
		// wywołujemy metody o takiej samej nazwie, lecz
		// posiadających inne argumenty
		// wykorzystujemy tutaj zatem metody przesłonione (overloading)
		System.out.println(liscPierwszy.name("Zenek"));
		System.out.println(liscDrugi.name());
		System.out.println(liscTrzeci.name());
		
		// Dla zmiennej liscCzwarty przypisujemy wartość zwracaną przez metodę:
		// getNewInstance() - metoda ta zwraca nowy obiekt 
		// gdy byśmy użyli metody getInstance() - zostałaby
		// przypisana do zmiennej 'liscCzwarty' referencja do obiektu pierwszego
		// czyli zmiennaliscCzwarty wskazywalaby na liscPierwszy
		// ale nie bylaby osobnym, nowym lisciem
		Leaf liscCzwarty = liscPierwszy.getNewInstance();
		
		// dla potwierdzenia teorii, sprawdzamy jaki kolor
		// zostanie wyswietlony w zaleznosci od wywołania metod
		// getInstance() lub getNewInstance()
		System.out.println("\tKolor czwarty: " + liscCzwarty.color);
		
		// deklarujemy liczbe, ktora jest mniejsza od 10
		int agent = 7;
		
		// sprawdzamy dzialanie statycznej metody leadZero()
		// dla przekazanego argumentu
		System.out.println(OOP.leadZero(agent));
	}
	
	// w klasie OOP definiujemy kolejną metodę, 
	// która przyjmuje liczbę całkowitą (int)
	// i zwraca ją w postaci łańcucha znaków (String)
	// zasada działania metody jest taka,
	// ze powinna dodawać przed liczbą "0" - jeśli ta jest cyfrą
	public static String leadZero(int num) {
		// sprawdzamy czy liczba jest cyfra
		// cyfry zawierają się w przedziale <0, 9>
		if(num < 10) {
			// jezeli nasza liczba jest cyfrą, doklejamy z przodu "0"
			return "0" + num;
		}
		// w przeciwnym wypadku rzutujemy liczbę na typ String
		// do rzutowania wykorzystujemy tutaj konkatenację
		// typu prostego z łańcuchem znaków
		return num + "";
	}

}
