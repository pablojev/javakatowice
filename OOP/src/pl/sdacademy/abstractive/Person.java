package pl.sdacademy.abstractive;

public abstract class Person {
	private String name;
	
	public abstract void greet();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
