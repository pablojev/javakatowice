package pl.sdacademy.abstractive;

public class PolskiPerson extends Person {

	public PolskiPerson(String name) {
		super.setName(name);
	}
	
	@Override
	public void greet() {
		System.out.println("Witaj, jestem " + super.getName());
		
	}

}
