package pl.sdacademy.abstractive;

public class EnglishPerson extends Person {

	public EnglishPerson(String name) {
		super.setName(name);
	}
	
	@Override
	public void greet() {
		System.out.println("Welcome, I'm " + super.getName());
	}


}
