package pl.sdacademy.enums.one;

public enum EyeColor {
	BROWN, BLUE, GREEN, GREY;
}
