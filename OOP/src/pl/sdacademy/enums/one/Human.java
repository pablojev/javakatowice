package pl.sdacademy.enums.one;

public class Human {
	private String name;
	private EyeColor eye;
	
	public Human(String name, EyeColor eye) {
		this.name = name;
		this.eye = eye;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EyeColor getEye() {
		return eye;
	}

	public void setEye(EyeColor eye) {
		this.eye = eye;
	}
	
}
