package pl.sdacademy.enums.two;

public enum Car {
	FORD_FIESTA(1999),
	FIAT_MULTIPLA(2000),
	AUDI_TT(2003),
	SKODA_FABIA(1997),
	FORD_GALAXY(1995);
	
	private int year;
	
	private Car(int year) {
		this.year = year;
	}
	
	public int getYear() {
		return this.year;
	}
}
