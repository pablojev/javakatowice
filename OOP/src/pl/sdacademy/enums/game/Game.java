package pl.sdacademy.enums.game;

import java.util.Random;
import java.util.Scanner;

public class Game {

	private Scanner sc;
	private Choices computerChoice;
	private Choices userChoice;
	
	public Game() {
		this.sc = new Scanner(System.in);
	}
	
	public void start() {
		String randomChoiceStr = "" + new Random().nextInt(3);
		this.computerChoice = this.convertChoice(randomChoiceStr);
		String userChoiceStr = this.getUserInput();
		// TODO: zabezpieczenie, dopóki uzytkownik nie poda prawidłowej watości
		this.userChoice = this.convertChoice(userChoiceStr);
		
		Status s = null;
		
		switch(this.computerChoice) {
			case ROCK:
				s = (this.userChoice == Choices.ROCK) ? Status.TIE : 
					(this.userChoice == Choices.PAPER) ? Status.WIN : Status.LOOSE;
				break;
			case PAPER:
				s = (this.userChoice == Choices.ROCK) ? Status.LOOSE : 
					(this.userChoice == Choices.PAPER) ? Status.TIE : Status.WIN;
				break;
			case SCISSORS:
				s = (this.userChoice == Choices.ROCK) ? Status.WIN : 
					(this.userChoice == Choices.PAPER) ? Status.LOOSE : Status.TIE;
				break;
		}
		
		System.out.println(s.getCommonName().toUpperCase());
		System.out.println("[computer=" + this.computerChoice.getCommonName() + ", "
				+ "user=" + this.userChoice.getCommonName() + "]");
	}
	
	private String getUserInput() {
		System.out.println("Podaj swój wybór: ");
		return sc.nextLine();
	}
	
	private Choices convertChoice(String choice) {
		switch(choice) {
			case "0": 
			case "kamień":
				return Choices.ROCK;
			case "1":
			case "papier":
				return Choices.PAPER;
			case "2":
			case "nożyce":
				return Choices.SCISSORS;
			default:
				return null;
		}
	}
}
