package pl.sdacademy.enums.game;

public enum Status {
	WIN("wygrana"), LOOSE("przegrana"), TIE("remis");
	
	private String commonName;
	
	private Status(String commonName) {
		this.commonName = commonName;
	}
	
	public String getCommonName() {
		return this.commonName;
	}
}
