package pl.sdacademy.enums.game;

public enum Choices {
	ROCK("kamień"), 
	PAPER("papier"), 
	SCISSORS("nożyce");
	
	private String commonName;
	
	private Choices(String commonName) {
		this.commonName = commonName;
	}
	
	public String getCommonName() {
		return this.commonName;
	}
}
