package pl.sdacademy.oop;

public class YellowMapleLeaf extends MapleLeaf {

	
	public void superFly() {
		super.deatach();
	}

	@Override
	public String toString() {
		return "YellowMapleLeaf [bars=" + bars + ", color=" + color + ", size=" + size + ", name=" + name + ", length="
				+ length + ", name()=" + name() + ", whatIsMyColour()=" + whatIsMyColour() + ", getInstance()="
				+ getInstance() + ", getNewInstance()=" + getNewInstance() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
}
