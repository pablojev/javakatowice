// na początku pliku umieszczamy definicję, 
// w którym pakiecie znajduje się nasza klasa
package pl.sdacademy.oop;

// nastepnie mogą znaleźć się importy klas z innych pakietów

// definiujemy klasę 
public class Leaf {

	// w klasie definiujemy pola klasy
	// deklaracja powinna wyglądać następująco:
	// [modyfikato_dostepu] (opcjonalnie static, final) [typ] [nazwa_zmiennej]
	public String color;
	public String size;
	public String name;
	public double length;
	
	// W każdej klasie znajduje się konstruktor, czyli metoda
	// wywoływana automatycznie przy tworzeniu instancji obiektu. 
	// Dla konstruktora nie definiujemy typu zwracanych danych
	// Gdyż jego typem zawsze będzie zwrócenie instancji obiektu (w tym przypadku Leaf)
	
	// domyślny konstruktor jest publiczny i nie zawiera instrukcji.
	// jeżeli w klasie nie zdefiniujemy żadnego konstruktora 
	// na poziomie kompilacji automatycznie zostanie dodany pusty publiczny konstruktor
	public Leaf() { }
	
	// konstruktor tak naprawde jest metoda,
	// a wiec mozemy go przeslaniac zmieniajac parametry 
	// przyjmowane przezeń.
	public Leaf(String name) {
		this.name = name;
		
	}
	
	// tworząc kilka konstruktorów, które pozwolą nam
	// na utworzenie instancji z przekazanym parametrem danego typu
	public Leaf(double len) {
		this.length = len;
	} 
	
	// W klasie mamy możliwość definicji metod,
	// czyli akcji wykonywanych przez dany obiekt
	
	// w ponizszym przykladzie metoda fly(), nie zwraca zadnych danych (void)
	public void fly() {
		// i wydrukuje napis "Lece!!" do konsoli
		System.out.println("Lece!!");
	}
	
	// oczywiście nie jesteśmy ograniczeni ilością metod
	// możemy ich definiować dowolnie wiele
	public void rot() {
		System.out.println("Ojaacie zgnilem :(");
	}
	
	// kazda metoda sklada sie z nagłowka metody
	// public void deatach();
	
	// oraz ciała metody:
	// { System.out.println("Jenyyy, spadłem :/"); }
	
	public void deatach() {
		System.out.println("Jenyyy, spadłem :/");
	}
	
	// metody mogą także zwracać dane, które możemy przypisać
	// do pewnej zmiennej - wtedy inaczej wygląda nagłówek takiej metody
	public String name(String name) {
		return "Czesc, jestem lisciem i nazywam sie " + name;
		// kod, który zadeklarujemy po instrukcji 'return' nie wykona się,
		// gdyż jest nieosiągalny dla kompilatora - instrukcja return wychodzi z f-cji
	}
	
	// metody możemy przesłaniać
	// oznacza to, że możemy napisać metodę o takiej samej nazwie,
	// ale przyjmującą inną liczbę argumentów, bądź argumenty innego typu
	// tak, aby kompilator wiedział, użyć
	// Zjawisko takie nosi nazwę Overloading (przeciążanie) [metody]
	public String name() {
		return this.name(name);
	}
	
	public String whatIsMyColour() {
		// jezeli chcemy w metodzie odwołać się do pola w klasie 
		// powinniśmy poprzedzić nazwę pola słowem kluczowym this
		return "Kolor to: " + this.color;
	}
	
	public void printColor(String color) {
		// w metodzie, odwołując się do zmiennej 'color', 
		// kiedy takiej oczekujemy w nagłówku metody jako definicji jej parametru
		// odwołujemy się normalnie po nazwie zmiennej.
		System.out.println(color);
	}
	
	// Metoda getInstance(),
	// zwraca aktualną instancję naszego obiektu
	// stad więc wniosek, że cała instancja naszego obiektu
	// jest reprezentowana za pomocą 'this' w klasie
	public Leaf getInstance() {
		return this;
	}
	
	// Zwracając nowy obiekt klasy
	// deklarujemy nowe miejsce w pamięci
	// i jest to zupełnie nowy obiekt
	// - nie posiada wypełnionych wartości w polach
	// zgodnie z naszym obiektem, na którym wywołujemy metodę
	public Leaf getNewInstance() {
		return new Leaf("");
	}
}
