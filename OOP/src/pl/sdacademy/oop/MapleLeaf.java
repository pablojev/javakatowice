package pl.sdacademy.oop;

public class MapleLeaf extends Leaf {

	public int bars;
	
	public MapleLeaf() {
		super("liść klonu");
		System.out.println("Utworzylem nowy lisc klonu");
	}
	
	public void showMeFlying() {
		System.out.println("\tLiść klonu (" + super.name + ") lata tak:");
		super.fly();
	}
}
