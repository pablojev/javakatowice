package pl.sdacademy.enumerable;

public class Colors {

	private ColorType color;
	
	public Colors() { }

	public ColorType getColor() {
		return color;
	}

	public void setColor(ColorType color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Colors [color=" + color + "]";
	}

	
	
}
