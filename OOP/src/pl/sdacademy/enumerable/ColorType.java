package pl.sdacademy.enumerable;

public enum ColorType {
	RED(1, "czerwony"), 
	GREEN(2, "zielony"), 
	BLUE(3, "niebieski"), 
	BROWN(4, "brazowy"), 
	BLACK(5, "czarny"), 
	WHITE(6, "biały");
	
	private String colorName;
	private int id;
	
	private ColorType(int id, String colorName) {
		this.id = id;
		this.colorName = colorName;
	} 
	
	public String getColorName() {
		return this.colorName;
	}
	
	public int getId() {
		return this.id;
	} 
	
}
